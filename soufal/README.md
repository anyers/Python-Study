## 掘金-Python记录
### Day 1（2018年10月15日）
#### **Q：**
安装Python3后，翻墙，得到`print('hello, world')`输出截图。    
#### **A:**
![print](https://images.gitee.com/uploads/images/2018/1015/151514_6d326660_2054580.jpeg "11111.jpg")

### Day 2（2018年10月16日）
#### **Q：**      
1. 安装一个代码编辑器，回复利用编辑器编写并运行以下代码的截图：

```
company,date,*others = ['juejin','20181016','morning','Tues']
print(company,date,others)
```
   
2. 自行准备教材「笨办法学 Python3」 作者：泽德 A. 肖（Zed A.Shaw）.        
#### **A:**      
1. 结果如下：
![1](https://images.gitee.com/uploads/images/2018/1016/140358_db684633_2054580.png "34.PNG")      




### Day 3（2018年10月17日）
#### **Q：**    
1. 掌握 Python 变量，输入输出，+-*/%运算
2. 编写 Python 程序，此程序可以从命令行接收一个数字输入，并输出以该数字为半径的圆的周长和面积。

#### **A:**   
![3](https://images.gitee.com/uploads/images/2018/1017/133912_3d10645e_2054580.png "3.PNG")      


### Day 4（2018年10月18日）
#### **Q：** 
1. 掌握 Python 的 if-else 分支结构以及 while for 循环（包括 continue 和 break)
2. 编写一个程序，该程序可以从命令行接收一个数字输入并判断该数字是否为素数。      


#### **A:**  
![4](https://images.gitee.com/uploads/images/2018/1018/102542_083acdbf_2054580.png "4.PNG")      

### Day 5（2018年10月19日）
#### **Q：** 
1. 了解 Python 的基本数据结构和函数
2. 将第二课中的程序改写成函数的形式。任意编写一个 List 作为函数的参数，判断 List 中的每个元素是否为素数。并将是素数的元素打印为字典。（可以随意设置 key） 


#### **A:**  
![5](https://images.gitee.com/uploads/images/2018/1019/135829_ce72791b_2054580.png "5.PNG")



### Day 6（2018年10月21日）
#### **Q：** 
1. 掌握链接中 Python 教程的高级特性
2. 利用生成器生成斐波那契数列

#### **A:**  
![6](https://images.gitee.com/uploads/images/2018/1021/152251_d44f0339_2054580.png "6.PNG")


### Day 7（2018年10月21日）
#### **Q：** 
1. 回顾前面所学的内容
2.在无参考资料的前提下，尽量多的写下你能回忆起的 Python 知识。


#### **A:**  
掘金链接：
[打开链接](https://juejin.im/post/5bcc2555e51d4560a870cb1f)      
思维导图：   
![g1](https://images.gitee.com/uploads/images/2018/1021/151719_d05d40f9_2054580.jpeg "Python 数据类型.jpg")
![g2](https://images.gitee.com/uploads/images/2018/1021/152150_3cba1227_2054580.jpeg "Python高级特性.jpg")




### Day 8（2018年10月22日）
#### **Q：** 
1. 利用装饰器给之前课程所写的判断素数的程序增加一个「有趣的」功能。

#### **A:**  
![8](https://images.gitee.com/uploads/images/2018/1022/185308_173f597b_2054580.png "7.PNG")
