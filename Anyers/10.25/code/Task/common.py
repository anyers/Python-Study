# -- coding: utf-8 --

from datetime import datetime
import functools

def log(func):	# 打印日志的高级函数
	@functools.wraps(func)
	def wrapper(*args, **kw):
		print(f'call function : {func.__name__}, args is {args}s' )
		return func(*args, **kw)
	return wrapper

def run(func):
	@functools.wraps(func)
	def run_time(*args, **kw): # 定义内部函数run_time，它接受任意个定位参数
		start_time = datetime.now()
		result = func(*args, **kw)
		print(f'[{datetime.now()}] {func.__name__} run time is {datetime.now() - start_time}')
		return result
	return run_time # 返回内部函数，取代被装饰的函数