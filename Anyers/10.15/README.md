# 【战胜拖延症，组团学 Python】- 第一天

## [今日打卡](https://juejin.im/pin/5bc400105188250d2520d3ef)

> 【战胜拖延症，组团学 Python】准备工作：
>
> ​	1.安装 Python3
>
> ​	2.配置翻墙
>
> 回复 print('hello, world') 输出截图，即可成功打开。

### 任务1

```python
print ('hello, world!!!');
```

![print_hello_world](./img/print_hello_world.png)

### 任务2

使用SSR...



---

## 总结

完成`Python3`的安装，因为之前安装的`Python2.7`，这里为了两个版本混用，使用`Anaconda`实现两个版本的共存和切换。



参考阅读：

- [使用Anaconda实现Python2和Python3共存及相互转换](https://zhuanlan.zhihu.com/p/36538425)

![conda命令速查表](./img/conda-cheatsheet.png)

