# 【战胜拖延症，组团学 Python】- 第六天

## [今日打卡](https://juejin.im/pin/5bcaeb5a6fb9a04d63f32ea9)

>【战胜拖延症，组团学 Python】第四课：
>1. 掌握链接中 Python 教程的高级特性
>2. 利用生成器生成斐波那契数列
>
>
>
>阅读链接中的函数部分配合搜索可以完成此任务。[廖雪峰Python教程 高级特性](https://www.liaoxuefeng.com/wiki/001374738125095c955c1e6d8bb493182103fac9270762a000/0013868196169906eb9ca5864384546bf3405ae6a172b3e000)

### 任务1

```python
# -- coding: utf-8 --

```

---

### 任务2

```python
# 斐波那契数列由 0 和 1 开始，之后的斐波那契数列系数就由之前的两数相加。
def fibonacci(num):
	n, a, b = 0, 0, 1
	while n < num:
		yield b
		a, b = b, a + b
		n += 1
```

---

### 参考阅读

- [斐波那契数列 - 维基百科](https://zh.wikipedia.org/wiki/%E6%96%90%E6%B3%A2%E9%82%A3%E5%A5%91%E6%95%B0%E5%88%97)