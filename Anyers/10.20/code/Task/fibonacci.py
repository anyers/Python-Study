# -- conding:utf-8 ---

'''
	利用生成器生成斐波那契数列

	斐波那契数列由 0 和 1 开始，之后的斐波那契数列系数就由之前的两数相加。
'''

def fibonacci(num):
	n, a, b = 0, 0, 1
	while n < num:
		yield b
		a, b = b, a + b
		n += 1

def fib(n):   # return Fibonacci series up to n
	result = []
	a, b = 0, 1
	while b < n:
		result.append(b)
		a, b = b, a+b
	return result

num = int(input("请输入生成斐波那契数列位数："))
fib_list = []
for i in fibonacci(num):
	fib_list.append(i)
print(fib_list)
# print(fib(num))