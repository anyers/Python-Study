# -- coding: utf-8 --

# 十进制转其它进制
print(f'{bin(1)}, {bin(64)}, {bin(255)}')
print(f'{oct(1)}, {oct(64)}, {oct(255)}')
print(f'{hex(1)}, {hex(64)}, {hex(255)}')

# 其他进制转十进制
print(f"{int('0b1', 2)}, {int('0b1000000', 2)}, {int('0b11111111', 2)}")
print(f"{int('0o1', 8)}, {int('0o100', 8)}, {int('0o377', 8)}")
print(f"{int('0x1', 16)}, {int('0x40', 16)}, {int('0xff', 16)}")

# 十进制转ASCII
for i in range(65, 72):
	print(f"{i} - '{chr(i)}'")

# ASCII转十进制
for s in ['A','B','C','D','E','F','G']:
	print(f"'{s}' - {ord(s)}")