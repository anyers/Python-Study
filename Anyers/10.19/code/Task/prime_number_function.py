# -- coding: utf-8 --

'''
将第二课中的程序改写成函数的形式。任意编写一个 List 作为函数的参数，
判断 List 中的每个元素是否为素数。并将是素数的元素打印为字典。（可以随意设置 key）

'''

#	判断素数概念 质数又称素数。 指在一个大于1的自然数中，除了1和此整数自身外，不能被其他自然数整除的数。
def prime_number(num_list):
	try:
		# 定义字典 key为prime, value为素数列表
		prime_dic = {"prime":[]};
		for num_str in num_list:
			num = int(num_str)
			for i in range(2, num):
				if num % i == 0:
					break
			else:
				print(f"{num} ，是素（质）数")
				prime_dic["prime"].append(num)
		print(f"输入的列表{num_list}中素数有: {prime_dic}")
	except Exception as e:
		print("程序执行异常：{}".format(e))

num_list = list(input("请输入多个数字，每个数字空格隔开：\n").split(' '));
prime_number(num_list)


