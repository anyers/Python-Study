# 【战胜拖延症，组团学 Python】- 第五天

## [今日打卡](https://juejin.im/pin/5bc945ddf265da6aedfc9161)

>【战胜拖延症，组团学 Python】第三课：
>
>​	1、了解 Python 的基本数据结构和函数
>
>​	2、将第二课中的程序改写成函数的形式。任意编写一个 List 作为函数的参数，判断 List 中的每个元素是否为素数。并将是素数的元素打印为字典。（可以随意设置 key）
>
>
>
>阅读链接中的函数部分配合搜索可以完成此任务。[廖雪峰Python教程 函数](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/00143167832686474803d3d2b7d4d6499cfd093dc47efcd000)

### 任务1

```python
# -- coding: utf-8 --

```



---

### 任务2

```python
# -- coding: utf-8 --
'''
将第二课中的程序改写成函数的形式。任意编写一个 List 作为函数的参数，
判断 List 中的每个元素是否为素数。并将是素数的元素打印为字典。（可以随意设置 key）

'''

#	判断素数概念 质数又称素数。 指在一个大于1的自然数中，除了1和此整数自身外，不能被其他自然数整除的数。
def prime_number(num_list):
	try:
		# 定义字典 key为prime, value为素数列表
		prime_dic = {"prime":[]};
		for num_str in num_list:
			num = int(num_str)
			for i in range(2, num):
				if num % i == 0:
					break
			else:
				print(f"{num} ，是素（质）数")
				prime_dic["prime"].append(num)
		print(f"输入的列表{num_list}中素数有: {prime_dic}")
	except Exception as e:
		print("程序执行异常：{}".format(e))

num_list = list(input("请输入多个数字，每个数字空格隔开：\n").split(' '));
prime_number(num_list)
```
