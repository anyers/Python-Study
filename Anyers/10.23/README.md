# 【战胜拖延症，组团学 Python】- 第九天

## [今日打卡](https://juejin.im/pin/5bce83d9e51d455be059fa97)

>【战胜拖延症，组团学 Python】第七课：
>
>1. 有很多人反馈难度有点大，但实际上还可以。今日继续下面的题吧
>2. 阅读链接中的函数式编程部分（到了比较有意思的地方了）。
>3. 利用装饰器给之前课程所写的判断素数的程序增加一个「有趣的」功能。
>
>将你写的程序源码及输出截图在评论下回复即可打卡。[廖雪峰Python教程 函数式编程](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/0014317848428125ae6aa24068b4c50a7e71501ab275d52000)

### 任务3

```python
# -- coding: utf-8 --

'''
	利用装饰器给之前课程所写的判断素数的程序增加一个「有趣的」功能
'''
def run(func):
	@functools.wraps(func)
	def run_time(*args, **kw): # 定义内部函数run_time，它接受任意个定位参数
		start_time = datetime.now()
		result = func(*args, **kw)
		print(f'[{datetime.now()}] {func.__name__} run time is {datetime.now() - start_time}')
		return result
	return run_time # 返回内部函数，取代被装饰的函数

@run
@log
def prime_number(num_list):
	try:
		# 定义字典 key为prime, value为素数列表
		prime_dic = {"prime":[]};
		for num_str in num_list:
			num = int(num_str)
			for i in range(2, num):
				if num % i == 0:
					break
			else:
				print(f"{num} ，是素（质）数")
				prime_dic["prime"].append(num)
		print(f"输入的列表{num_list}中素数有: {prime_dic}")
	except Exception as e:
		print("程序执行异常：{}".format(e))
```

---

### 参考阅读

- [流畅的Python](https://gitee.com/alex_d/Python-Study/attach_files)

