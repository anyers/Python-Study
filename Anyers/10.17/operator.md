## Python运算符

### 算术运算符

假设变量： **a=10，b=3**：

| 运算符 | 描述                                            | 实例                              |
| ------ | ----------------------------------------------- | --------------------------------- |
| +      | 加 - 两个对象相加                               | a + b 返回 13                     |
| -      | 减 - 得到负数或是一个数减去另一个数             | a - b 返回 7                      |
| *      | 乘 - 两个数相乘或是返回一个被重复若干次的字符串 | a * b 返回 30                     |
| /      | 除 - x除以y                                     | a / b 返回 3.3333333333333335     |
| %      | 取模 - 返回除法的余数                           | a % b 返回 1                      |
| **     | 幂 - 返回x的y次幂                               | a**b  返回 1000                   |
| //     | 取整除 - 返回商的整数部分（**向下取整**）       | 10//3 返回 3 , 10.0//3.0 返回 3.0 |

`Python 3` 命令行演示算术运算符操作：

```python
Python 3.7.0 (default, Jun 28 2018, 08:04:48) [MSC v.1912 64 bit (AMD64)] :: Anaconda, Inc. on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> 10 + 3
13
>>> 10 - 3
7
>>> 10 * 3
30
>>> 10 / 3
3.3333333333333335
>>> 10 % 3
1
>>> 10 ** 3
1000
>>> 10 // 3
3
>>> 10.0 // 3
3.0
```

**注意：**Python2.x 里，整数除整数，返回为整数。如果要得到小数部分，把其中一个数改成浮点数即可（自动进行了类型转换）。

```python
Python 2.7.15 |Anaconda, Inc.| (default, May  1 2018, 18:37:09) [MSC v.1500 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> 10 / 3
3
>>> 10.0 /3
3.3333333333333335
>>> 10 / 3.0
3.3333333333333335
>>> 10 / float(3)
3.3333333333333335
```

---

### 比较运算符

假设变量： **a=10，b=3**：

| 运算符 | 描述                                                         | 实例                                     |
| ------ | ------------------------------------------------------------ | ---------------------------------------- |
| ==     | 等于 - 比较对象是否相等                                      | (a == b) 返回 False。                    |
| !=     | 不等于 - 比较两个对象是否不相等                              | (a != b) 返回 True.                      |
| <>     | 不等于 - 比较两个对象是否不相等 (**Python 2可用**)           | (a <> b) 返回 True。这个运算符类似 != 。 |
| >      | 大于 - 返回x是否大于y                                        | (a > b) 返回 False。                     |
| <      | 小于 - 返回x是否小于y。所有比较运算符返回1表示真，返回0表示假。这分别与特殊的变量True和False等价。 | (a < b) 返回 True。                      |
| >=     | 大于等于	- 返回x是否大于等于y。                           | (a >= b) 返回 False。                    |
| <=     | 小于等于 - 返回x是否小于等于y。                              | (a <= b) 返回 True。                     |

`Python 3 `命令行演示比较运算符操作：

```python
Python 3.7.0 (default, Jun 28 2018, 08:04:48) [MSC v.1912 64 bit (AMD64)] :: Anaconda, Inc. on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> 10 == 3
False
>>> 10 != 3
True
>>> 10 <> 3
  File "<stdin>", line 1
    10 <> 3
        ^
SyntaxError: invalid syntax
>>> 10 > 3
True
>>> 10 < 3
False
>>> 10 >= 3
True
>>> 10 <= 3
False
```

```python
Python 2.7.15 |Anaconda, Inc.| (default, May  1 2018, 18:37:09) [MSC v.1500 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> 10 <> 3
True
```

---

### 赋值运算符

假设变量： **a=10，b=3, c=0**：

| 运算符 | 描述             | 实例                                  |
| ------ | ---------------- | ------------------------------------- |
| =      | 简单的赋值运算符 | c = a + b 将 a + b 的运算结果赋值为 c |
| +=     | 加法赋值运算符   | c += a 等效于 c = c + a               |
| -=     | 减法赋值运算符   | c -= a 等效于 c = c - a               |
| *=     | 乘法赋值运算符   | c *= a 等效于 c = c * a               |
| /=     | 除法赋值运算符   | c /= a 等效于 c = c / a               |
| %=     | 取模赋值运算符   | c %= a 等效于 c = c % a               |
| **=    | 幂赋值运算符     | c **= a 等效于 c = c ** a             |
| //=    | 取整除赋值运算符 | c //= a 等效于 c = c // a             |

`Python 3 ` 演示比较运算符操作：

```python
a = 10; b = 3; c = 0

c = a + b
print("[c = a + b]  c=", c)

c += a
print("[c += a] c=", c) 

c -= a
print("[c -= a] c=", c) 

c *= a
print("[c *= a] c=", c) 

c /= a 
print("[c /= a ] c=", c) 

c %= a
print("[c %= a] c=", c) 

c **= a
print("[c **= a] c=", c)
 
c //= a
print("[c //= a] c=", c)

--- result ---
[c = a + b]  c= 13
[c += a] c= 23
[c -= a] c= 13
[c *= a] c= 130
[c /= a ] c= 13.0
[c %= a] c= 3.0
[c **= a] c= 59049.0
[c //= a] c= 5904.0
```

---

### 位运算符

假设变量： **a=10，b=3**：

| 运算符 | 描述                                                         | 实例                                                         |
| ------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| &      | 按位与运算符：参与运算的两个值,如果两个相应位都为1,则该位的结果为1,否则为0 | (a & b) 返回 2 ，二进制：0b10                                |
| \|     | 按位或运算符：只要对应的二个二进位有一个为1时，结果位就为1。 | (a \| b) 返回 11 ，二进制：0b1011                            |
| ^      | 按位异或运算符：当两对应的二进位相异时，结果为1              | (a ^ b) 返回 9 ，二进制： 0b1001                             |
| ~      | 按位取反运算符：对数据的每个二进制位取反,即把1变为0,把0变为1 。~x 类似于 -x-1 | (~a ) 返回 -1 ，二进制： -0b1011 ，在一个有符号二进制数的补码形式。 |
| <<     | 左移动运算符：运算数的各二进位全部左移若干位，由 << 右边的数字指定了移动的位数，高位丢弃，低位补0。 | a << b 返回 80，二进制： 0b1010000                           |
| >>     | 右移动运算符：把">>"左边的运算数的各二进位全部右移若干位，>> 右边的数字指定了移动的位数 | a >> b 返回 1，二进制：0b1                                   |

`Python 3 ` 演示比较运算符操作：

```python
a = 10; b = 3; c = 0;

c = a & b
print("[a & b] c={0}, bin(c)={1}".format(c, bin(c)))
c = a | b
print("[a | b] c={0}, bin(c)={1}".format(c, bin(c)))
c = a ^ b
print("[a ^ b] c={0}, bin(c)={1}".format(c, bin(c)))
c = ~a
print("[a ~ b] c={0}, bin(c)={1}".format(c, bin(c)))
c = a << b
print("[a << b] c={0}, bin(c)={1}".format(c, bin(c)))
c = a >> b
print("[a >> b] c={0}, bin(c)={1}".format(c, bin(c)))

--- result ---

[a & b] c=2, bin(c)=0b10
[a | b] c=11, bin(c)=0b1011
[a ^ b] c=9, bin(c)=0b1001
[a ~ b] c=-11, bin(c)=-0b1011
[a << b] c=80, bin(c)=0b1010000
[a >> b] c=1, bin(c)=0b1
```

---

### 逻辑运算符

假设变量： **a=10，b=3**：

| 运算符 | 逻辑表达式 | 描述                                                         | 实例                    |
| ------ | ---------- | ------------------------------------------------------------ | ----------------------- |
| and    | x and y    | 布尔"与" - 如果 x或y 为 False（小于等于0），x and y 返回 False（返回x或y中为False的值），否则它返回 y 的值。 | (a and b) 返回 20。     |
| or     | x or y     | 布尔"或"	- 如果 x 是非 0，它返回 x 的值，否则它返回 y 的值。 | (a or b) 返回 10。      |
| not    | not x      | 布尔"非" - 如果 x 为 True，返回 False 。如果 x 为 False，它返回 True。 | not(a and b) 返回 False |

`Python 3 ` 演示比较运算符操作：

```python
Python 3.7.0 (default, Jun 28 2018, 08:04:48) [MSC v.1912 64 bit (AMD64)] :: Anaconda, Inc. on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> 10 and 3
3
>>> 0 and 3
0
>>> 0 and 0
0
>>> False and 3
False
>>> True and 3
3
>>> 10 and False
False
>>> 10 and True
True
>>> 10 or 3
10
>>> False or 3
3
>>> True or 3
True
>>> 10 or False
10
>>> 10 or True
10
>>> not 10
False
>>> not 3
False
>>> not (10 and 3)
False
```

---

### 成员运算符

假设变量： **a=10，b=3, list = [1,2,3,4,5]，set = (1,2,3,4,5)，dic = [1,2,3,4,5]**：

| 运算符 | 描述                                                    | 实例                                              |
| ------ | ------------------------------------------------------- | ------------------------------------------------- |
| in     | 如果在指定的序列中找到值返回 True，否则返回 False。     | x 在 y 序列中 , 如果 x 在 y 序列中返回 True。     |
| not in | 如果在指定的序列中没有找到值返回 True，否则返回 False。 | x 不在 y 序列中 , 如果 x 不在 y 序列中返回 True。 |

`Python 3` 演示算术运算符操作：

```python
a = 10; b = 3; list = [1,2,3,4,5]; set = (1,2,3,4,5); dic = [1,2,3,4,5];

print("[a in list] =", (a in list))
print("[b in list] =", (b in list))
print("[a not in list] =", (a not in list))
print("[b not in list] =", (b not in list))

print("[a in set] =", (a in set))
print("[b in set] =", (b in set))
print("[a not in set] =", (a not in set))
print("[b not in set] =", (b not in set))

print("[a in dic] =", (a in dic))
print("[b in dic] =", (b in dic))
print("[a not in dic] =", (a not in dic))
print("[b not in dic] =", (b not in dic))

--- result ---
[a in list] = False
[b in list] = True
[a not in list] = True
[b not in list] = False

[a in set] = False
[b in set] = True
[a not in set] = True
[b not in set] = False

[a in dic] = False
[b in dic] = True
[a not in dic] = True
[b not in dic] = False
```

---

### 身份运算符

身份运算符用于比较两个对象的存储单元

假设变量： **a = 10; b = 3; c = [1,2]; d = 0.333**：

| 运算符 | 描述                                        | 实例                                                         |
| ------ | ------------------------------------------- | ------------------------------------------------------------ |
| is     | is 是判断两个标识符是不是引用自一个对象     | **x is y**, 类似 **id(x) == id(y)** , 如果引用的是同一个对象则返回 True，否则返回 False |
| is not | is not 是判断两个标识符是不是引用自不同对象 | **x is not y** ， 类似 **id(a) != id(b)**。如果引用的不是同一个对象则返回结果 True，否则返回 False。 |

`Python 3` 演示算术运算符操作：

```python
a = 10; b = 3; c = [1,2]; d = 0.333

print("[a is b] =", (a is b))
print("[a is 10] =", (a is 10))
print("[a is int(10)] =", (a is int(10)))
print("[c is [1,2]] =", (c is [1,2]))
print("[d is 10/3] =", (d is (10/3)))
print("[d is (0.3+0.033)] =", (d is (0.3+0.033)))

print("[b is not a] =", (b is not a))
print("[a is not 10] =", (a is not 10))
print("[a is not int(10)] =", (a is not int(10)))
print("[c is not [1,2]] =", (c is not [1,2]))
print("[d is not 10/3] =", (d is not (10/3)))
print("[d is not (0.3+0.033)] =", (d is not (0.3+0.033)))

--- result ---
[a is b] = False
[b is a] = False
[a is 10] = True
[a is int(10)] = True
[c is [1,2]] = False
[d is 0.333] = True   # IDE内返回True（两者ID相同）; 命令行内返回False（两者ID不同）
[d is 10/3] = False
[d is (0.3+0.033)] = False

[a is not b] = True
[b is not a] = True
[a is not 10] = False
[a is not int(10)] = False
[c is not [1,2]] = True
[d is not 0.333] = False
[d is not 10/3] = True
[d is not (0.3+0.033)] = True
```

> **问：** 为什么IDE内生成字面量生成的变量id和对应字面量值的id相同，而命令行内不同？
>
> ![1539769215556](.\img\cmd_param_id_deal.png)

---

### 运算符优先级

以下表格列出了从最高到最低优先级的所有运算符：

| 运算符                   | 描述                                                   |
| ------------------------ | ------------------------------------------------------ |
| **                       | 指数 (最高优先级)                                      |
| ~ + -                    | 按位翻转, 一元加号和减号 (最后两个的方法名为 +@ 和 -@) |
| * / % //                 | 乘，除，取模和取整除                                   |
| + -                      | 加法减法                                               |
| >> <<                    | 右移，左移运算符                                       |
| &                        | 位 'AND'                                               |
| ^ \|                     | 位运算符                                               |
| <= < > >=                | 比较运算符                                             |
| <> == !=                 | 等于运算符                                             |
| = %= /= //= -= += *= **= | 赋值运算符                                             |
| is is not                | 身份运算符                                             |
| in not in                | 成员运算符                                             |
| not and or               | 逻辑运算符                                             |

