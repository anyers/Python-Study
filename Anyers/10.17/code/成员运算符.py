a = 10; b = 3; list = [1,2,3,4,5]; set = (1,2,3,4,5); dic = [1,2,3,4,5];

print("[a in list] =", (a in list))
print("[b in list] =", (b in list))
print("[a not in list] =", (a not in list))
print("[b not in list] =", (b not in list))

print("[a in set] =", (a in set))
print("[b in set] =", (b in set))
print("[a not in set] =", (a not in set))
print("[b not in set] =", (b not in set))

print("[a in dic] =", (a in dic))
print("[b in dic] =", (b in dic))
print("[a not in dic] =", (a not in dic))
print("[b not in dic] =", (b not in dic))


