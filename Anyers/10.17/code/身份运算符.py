a = 10; b = 3; c = [1,2]; d = 0.333; 

print("[a is b] =", (a is b))
print("[b is a] =", (b is a))
print("[a is 10] =", (a is 10))
print("[a is int(10)] =", (a is int(10)))
print("[c is [1,2]] =", (c is [1,2]))
print("[d is 0.333] =", (d is 0.333))
print("id(d) = {0} , id(0.333) = {1}".format(id(d), id(0.333)))
print("[d is 10/3] =", (d is (10/3)))
print("[d is (0.3+0.033)] =", (d is (0.3+0.033)))

print("[a is not b] =", (a is not b))
print("[b is not a] =", (b is not a))
print("[a is not 10] =", (a is not 10))
print("[a is not int(10)] =", (a is not int(10)))
print("[c is not [1,2]] =", (c is not [1,2]))
print("[d is not 0.333] =", (d is not 0.333))
print("[d is not 10/3] =", (d is not (10/3)))
print("[d is not (0.3+0.033)] =", (d is not (0.3+0.033)))



