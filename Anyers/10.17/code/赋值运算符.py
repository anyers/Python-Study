# -*- coding: UTF-8 -*-

a = 10; b = 3; c = 0
 
c = a + b
print("[c = a + b] =", c)

c += a
print("[c += a] =", c) 

c -= a
print("[c -= a] =", c) 

c *= a
print("[c *= a] =", c) 

c /= a 
print("[c /= a ] =", c) 

c %= a
print("[c %= a] =", c) 

c **= a
print("[c **= a] =", c)
 
c //= a
print("[c //= a] =", c)