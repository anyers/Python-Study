# -- coding: utf-8 --

'''
    3.自己找个需要计算的东西，写一个 .py 文件把它计算出来。
'''

print('---计算器---')
num1 = float(input('请输入第一个数字:'))
num2 = float(input('请输入第二个数字:'))
opt = input('请输入运算符(+,-,*,/):')

#加
add = num1 + num2
#减
sub = num1 - num2
#乘
mul = num1 * num2
#除
div = num1 / num2

print('====================输出结果=====================')
if opt == '+':
    print('您输入的加法运算结果为: %f'%add)
elif opt == '-':
    print('您输入的减法运算结果为: %f'%sub)
elif opt == '*':
    print('您输入的乘法运算结果为: %f'%mul)
elif opt == '/':
    print('您输入的除法运算结果为: %f'%div)
else :
    print('您输入的运算符有误！')
print('================================================')
