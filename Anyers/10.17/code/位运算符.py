a = 10; b = 3; c = 0;

c = a & b
print("[a & b] ={0}, bin(c)={1}".format(c, bin(c)))
c = a | b
print("[a | b] ={0}, bin(c)={1}".format(c, bin(c)))
c = a ^ b
print("[a ^ b] ={0}, bin(c)={1}".format(c, bin(c)))
c = ~a
print("[a ~ b] ={0}, bin(c)={1}".format(c, bin(c)))
c = a << b
print("[a << b] ={0}, bin(c)={1}".format(c, bin(c)))
c = a >> b
print("[a >> b] ={0}, bin(c)={1}".format(c, bin(c)))
