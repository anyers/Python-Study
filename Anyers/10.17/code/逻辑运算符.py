# -- coding: utf-8 --

print("--------- and ----------")

a = 10; b = 3;

if (a and b):
	print("a和b都是true , [a and b] =", (a and b))
else:
	print("a和b有一个不为true , [a and b] =", (a and b))

if (b and a):
	print("a和b都是true , [b and a] =", (b and a))
else:
	print("a和b有一个不为true , [b and a] =", (b and a))

a = 0; b = 3;

if (a and b):
	print("a和b都是true , [a and b] =", (a and b))
else:
	print("a和b有一个不为true , [a and b] =", (a and b))

if (b and a):
	print("a和b都是true , [b and a] =", (b and a))
else:
	print("a和b有一个不为true , [b and a] =", (b and a))

a = 10; b = 0;

if (a and b):
	print("a和b都是true , [a and b] =", (a and b))
else:
	print("a和b有一个不为true , [a and b] =", (a and b))

if (b and a):
	print("a和b都是true , [b and a] =", (b and a))
else:
	print("a和b有一个不为true , [b and a] =", (b and a))

print("--------- or ----------")

a = 10; b = 3;

if (a or b):
	print("a和b至少一个为true , [a or b] =", (a or b))
else:
	print("a和b都不为true , [a or b] =", (a or b))

if (b or a):
	print("a和b至少一个为true , [b or a] =", (b or a))
else:
	print("a和b都不为true , [b or a] =", (b or a))

a = 0; b = 3;

if (a or b):
	print("a和b至少一个为true , [a or b] =", (a or b))
else:
	print("a和b都不为true , [a or b] =", (a or b))

if (b or a):
	print("a和b至少一个为true , [b or a] =", (b or a))
else:
	print("a和b都不为true , [b or a] =", (b or a))

a = 10; b = 0;

if (a or b):
	print("a和b至少一个为true , [a or b] =", (a or b))
else:
	print("a和b都不为true , [a or b] =", (a or b))

if (b or a):
	print("a和b至少一个为true , [b or a] =", (b or a))
else:
	print("a和b都不为true , [b or a] =", (b or a))

a = -1; b = 0;

if (a or b):
	print("a和b至少一个为true , [a or b] =", (a or b))
else:
	print("a和b都不为true , [a or b] =", (a or b))

if (b or a):
	print("a和b至少一个为true , [b or a] =", (b or a))
else:
	print("a和b都不为true , [b or a] =", (b or a))

a = 0; b = -1;

if (a or b):
	print("a和b至少一个为true , [a or b] =", (a or b))
else:
	print("a和b都不为true , [a or b] =", (a or b))

if (b or a):
	print("a和b至少一个为true , [b or a] =", (b or a))
else:
	print("a和b都不为true , [b or a] =", (b or a))


a = -1; b = -2;

if (a or b):
	print("a和b至少一个为true , [a or b] =", (a or b))
else:
	print("a和b都不为true , [a or b] =", (a or b))

if (b or a):
	print("a和b至少一个为true , [b or a] =", (b or a))
else:
	print("a和b都不为true , [b or a] =", (b or a))

