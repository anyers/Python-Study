# -- coding: utf-8 --

'''
	1、掌握 Python 变量，输入输出，+-*/%运算
	2、编写 Python 程序，此程序可以从命令行接收一个数字输入，并输出以该数字为半径的圆的周长和面积。
'''
import math;

def operator():
    try:
        num1 = float(input("请输入第一个变量："))
        num2 = float(input("请输入第二个变量："))
        print("num1 + num2 = {}".format(num1 + num2))
        print("num1 - num2 = {}".format(num1 - num2))
        print("num1 * num2 = {}".format(num1 * num2))
        print("num1 / num2 = {}".format(num1 / num2))
        print("num1 % num2 = {}".format(num1 % num2))
    except Exception as e:
        print("程序执行异常：{}".format(e))

def circle():
    try:
        radius = float(input("请输入圆的半径："))
        # 周长计算 = 2 * pi * r
        circumference = 2 * math.pi * radius
        # 面积计算 = pi * r ** 2
        area = 3 * math.pi ** 2
        print("半径：{}，圆的周长为：{}，圆的面积为{}".format(radius, circumference, area))
    except Exception as e:
        print("程序执行异常：{}".format(e));

operator();
circle();

