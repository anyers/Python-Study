# 【战胜拖延症，组团学 Python】- 第三天

## [今日打卡](https://juejin.im/pin/5bc69f2be51d455be059a92c)

> 【战胜拖延症，组团学 Python】第一课：
>
> ​	1.掌握 Python 变量，输入输出，+-*/%运算
>
> ​	2.编写 Python 程序，此程序可以从命令行接收一个数字输入，并输出以该数字为半径的圆的周长和面积。
> ​	
>
> 阅读 Learn Python3 the hardway  Exercise1 至 Exercise8 + Google 搜索可以完成此次打卡任务。

### 任务1

```python
# -- coding: utf-8 --
def operator():
    try:
        num1 = float(input("请输入第一个变量："))
        num2 = float(input("请输入第二个变量："))
        print("num1 + num2 = {}".format(num1 + num2))
        print("num1 - num2 = {}".format(num1 - num2))
        print("num1 * num2 = {}".format(num1 * num2))
        print("num1 / num2 = {}".format(num1 / num2))
        print("num1 % num2 = {}".format(num1 % num2))
    except Exception as e:
        print("程序执行异常：{}".format(e))

operator();
```

参考阅读：

- [Python格式化字符](./format_characters.md)
- [Python操作符](./operator.md)

---

### 任务2

```python
# -- coding: utf-8 --

import math;

def circle():
    try:
        radius = float(input("请输入圆的半径："))
        # 周长计算 = 2 * pi * r
        circumference = 2 * math.pi * radius
        # 面积计算 = pi * r ** 2
        area = 3 * math.pi ** 2
        print("半径：{}，圆的周长为：{}，圆的面积为{}".format(radius, circumference, area))
    except Exception as e:
        print("程序执行异常：{}".format(e));

circle();


```

---

## 扩展阅读

 - [Python及第三方库api查看](https://blog.csdn.net/u012995964/article/details/49276583) - 使用pydoc，效果如下

   ![pydoc生成文档界面](./img/python_doc_local.png)

 - [Python中文学习大本营](http://www.pythondoc.com/) - 各种Python中文文档

 - [Python 入门指南（ Python tutorial. Release 3.6.3） ](http://www.pythondoc.com/pythontutorial3/)