# -- coding: utf-8 --

'''
	利用装饰器给之前课程所写的判断素数的程序增加一个「有趣的」功能

'''

import functools

def log(func):	# 打印日志的高级函数
	@functools.wraps(func)
	def wrapper(*args, **kw):
		print(f'call function : {func.__name__}, args is {args}' )
		return func(*args, **kw)
	return wrapper

@log
def is_prime_number(num):	# 判断传入的数是否为素数，是素数返回ture,不是，返回false
	try:
		if num > 1:
			for i in range(2, num):
				if num % i == 0:
					print(f"您输入的 : {num} ，不是素（质）数")
					return False
			else:
				print(f"您输入的 : {num} ，是素（质）数")
				return True
		else:
			print(f"您输入的 : {num} ，不是素（质）数")
			return False
	except Exception as e:
		print("程序执行异常：{}".format(e))
		return False

@log
def prime_number(num_list):
	try:
		# 定义字典 key为prime, value为素数列表
		prime_dic = {"prime":[]};
		for num_str in num_list:
			num = int(num_str)
			for i in range(2, num):
				if num % i == 0:
					break
			else:
				print(f"{num} ，是素（质）数")
				prime_dic["prime"].append(num)
		print(f"输入的列表{num_list}中素数有: {prime_dic}")
	except Exception as e:
		print("程序执行异常：{}".format(e))

if __name__ == '__main__':
	
	# num = int(input("请输入数字（整数）："))
	# is_prime_number(num)
	
	num_list = list(input("请输入多个数字，每个数字空格隔开：\n").split(' '));
	prime_number(num_list)
	
