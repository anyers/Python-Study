# -- coding: utf-8 --

def square(x) :            # 计算平方数
	return x ** 2

square_list_1 = map(square, [1,2,3,4,5])   # 计算列表各个元素的平方
square_list_2 = map(lambda x: x ** 2, [1, 2, 3, 4, 5])  # 使用 lambda 匿名函数

print(f"map(square, [1,2,3,4,5]) => type:{type(square_list_1)}; list:{list(square_list_1)}")
print(f"map(lambda x: x ** 2, [1, 2, 3, 4, 5]) => type:{type(square_list_2)}; list:{list(square_list_2)}")
