# -- coding: utf-8 --

import math

# 过滤出列表中的所有奇数
def is_odd(n):			# ==> lambda(n : n % 2 == 1)
	return n % 2 == 1
 
# tmplist = filter(is_odd, range(1, 101))
# newlist = list(tmplist)
#print(f'filter(is_odd, range(1, 101)) => type:{tmplist}; list:{newlist}')


# 过滤出1~100中平方根是整数的数
def is_sqr(n):			# ==> lambda(n : math.sqrt(x) % 1 == 0)
	return math.sqrt(n) % 1 == 0

# tmplist = filter(is_sqr, range(1, 101))
# newlist = list(tmplist)
#print(f'filter(is_sqr, range(1, 101)) => type:{tmplist}; list:{newlist}')


# filter求素数（使用埃拉托色尼筛选法）
# 构造一个从3开始的奇数序列
def _odd_iter():
	n = 1
	while True:
		n = n + 2
		yield n

# 定义一个筛选函数(筛选掉传入参数的倍数值)
def _not_divisible(n):
	return lambda x: x % n > 0

# 定义一个生成器，不断返回下一个素数
def primes():
	yield 2
	it = _odd_iter() # 初始序列
	while True:
		n = next(it) # 返回序列的第一个数
		yield n
		it = filter(_not_divisible(n), it) # 构造新序列

if __name__ == '__main__':
	# 打印1000以内的素数:
	for n in primes():
		if n < 1000:
			print(n)
		else:
			break