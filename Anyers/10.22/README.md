# 【战胜拖延症，组团学 Python】- 第八天

## [今日打卡](https://juejin.im/pin/5bcd3a126fb9a04d63f338da)

>【战胜拖延症，组团学 Python】第六课：
>
> 	1. 阅读链接中的函数式编程部分（到了比较有意思的地方了）。
> 	2. 利用装饰器给之前课程所写的判断素数的程序增加一个「有趣的」功能。
>
>将你写的程序源码及输出截图在评论下回复即可打卡。[廖雪峰Python教程 函数式编程](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/0014317848428125ae6aa24068b4c50a7e71501ab275d52000)

### 任务1

[函数式编程](./functional_rogramming.md)

**参考阅读**

- [函数式编程 - 看云](https://www.kancloud.cn/kancloud/functional-programm-for-rest/56933) | [pdf](https://gitee.com/alex_d/Python-Study/attach_files)

---

### 任务2

```python
import functools

def log(func):	# 打印日志的高级函数
	@functools.wraps(func)
	def wrapper(*args, **kw):
		print(f'call function : {func.__name__}, args is {args}' )
		return func(*args, **kw)
	return wrapper

@log
def prime_number(num_list):
	try:
		# 定义字典 key为prime, value为素数列表
		prime_dic = {"prime":[]};
		for num_str in num_list:
			num = int(num_str)
			for i in range(2, num):
				if num % i == 0:
					break
			else:
				print(f"{num} ，是素（质）数")
				prime_dic["prime"].append(num)
		print(f"输入的列表{num_list}中素数有: {prime_dic}")
	except Exception as e:
		print("程序执行异常：{}".format(e))

if __name__ == '__main__':
	
	# num = int(input("请输入数字（整数）："))
	# is_prime_number(num)
	
	num_list = list(input("请输入多个数字，每个数字空格隔开：\n").split(' '));
	prime_number(num_list)
```
