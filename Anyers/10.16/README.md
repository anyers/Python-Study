# 【战胜拖延症，组团学 Python】- 第二天

## [今日打卡](https://juejin.im/pin/5bc55064f265da6aedfc798a)

> 【战胜拖延症，组团学 Python】准备工作（二）：
>
> ​	1.安装一个代码编辑器。（推荐 visual studio code ，安装 Python 插件）
>
> ​	2.自行准备教材「笨办法学 Python3」 作者：泽德 A. 肖（Zed A.Shaw）  。
>
> 回复利用编辑器编写并运行以下代码的截图：
> ​	company,date,*others = ['juejin','20181016','morning','Tues']
> ​	print(company,date,others)

### 任务1

- Visual Studio Code

  ![Visual Studio Code](./img/ide_vscode_test.png)

- Sublime Text 3

  ![Sublime Text 3](./img/Sublime_Text3_test.png)

- PyCharm

  ![PyCharm](./img/ide_pycharm_test.png)

经过尝试，还是比较喜欢用Sublime Text3及PyCharm，后续继续延用PyCharm。

参考阅读

- [Visual Studio Code汉化](https://blog.csdn.net/testcs_dn/article/details/75070415)

- [windows10环境下用anaconda和VScode配置](https://blog.csdn.net/u011622208/article/details/79625908)
- [VS Code：让你工作效率翻倍的23个插件和23个编辑技巧](https://juejin.im/post/5bc55606e51d450e853075c9)

---

### 任务2

![推荐Python书籍电子版](./img/study_file.png)

[「战胜拖延症，组团学 Python」](https://juejin.im/post/5bbdd6896fb9a05ce02a9902)活动文章推荐书籍电子版，已上传码云[仓库附件](https://gitee.com/alex_d/Python-Study/attach_files)

参考阅读

- [笨方法学Python3视频教程（Python3.6.0英文版） - 哔哩哔哩](https://www.bilibili.com/video/av25675370/)
- [笨方法学Python3学习文集 - 小白学Python文集 （Python3.6.0汉化版~）](https://www.jianshu.com/c/1c6a182448e5) - [爱学习的ai酱](https://www.jianshu.com/u/949b399ab14e)

