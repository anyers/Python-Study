## Python数据类型

Python 标准库用C 实现了丰富的序列类型，列举如下。

- 容器序列
  list、tuple 和collections.deque 这些序列能存放不同类型的数据。
- 扁平序列
  str、bytes、bytearray、memoryview 和array.array，这类序列只能容纳一种类型。

`容器序列` 存放的是任意类型的对象的引用， `扁平序列` 存放的是值而不是引用（扁平序列其实是一段连续的内存空间）。

序列类型还能按照能否被修改来分类。

- 可变序列
  list、bytearray、array.array、collections.deque 和memoryview。
- 不可变序列
  tuple、str 和bytes

![1540363852779](.\img\可变及不可变序列UML关系.png)

标准的数据类型

|                    | **数据类型**       | **说明**                                                     |
| ------------------ | ------------------ | ------------------------------------------------------------ |
| **不可变数据类型** | Number（数字）     | 数字数据类型用于存储数值。                                   |
|                    | String（字符串）   | 字符串或串(String)是由数字、字母、下划线组成的一串字符       |
|                    | Tuple（元组）      | 类似List的数据类型，不可变                                   |
| **可变数据类型**   | List（列表）       | 列表可以完成大多数集合类的数据结构实现。                     |
|                    | Dictionary（字典） | 由索引(key)和它对应的值value组成。                           |
|                    | Set（集合）        | 由一个或数个形态各异的大小整体组成的，构成集合的事物或对象称作元素或是成员。 |

---

### 数据类型查看 - type() & isinstance()

两者的区别:

- type()不会认为子类是一种父类类型。
- isinstance()会认为子类是一种父类类型。

[更多内容](./type_isinstance.md)

---

### Number 数据类型

Number数据类型是不可改变的数据类型，这意味着改变数字数据类型会分配一个新的对象。

```python
>>> a = 3; id(a); a = 4; id(a)
140719147315008
140719147315040
```

可以使用 `del`  语句删除一些数字对象的引用

```python
# del语句语法
# del var1[,var2[,var3[....,varN]]]]

>>> a = 3; id(a); del a; id(a)
140719147315008
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'a' is not defined
>>>
```

Python 支持 **int、float、complex（复数）**。（Python2有**Long**类型；Python3有**bool**类型

```python
>>> a, b, c, d = 20, 5.5, True, 4+3j
>>> print(type(a), type(b), type(c), type(d))
<class 'int'> <class 'float'> <class 'bool'> <class 'complex'>
#浮点型
>>> 10 + 0.1, 10 / 2, 10 / 3
(10.1, 5.0, 3.3333333333333335)
# bool类型
>>> True + True,  True + False, False + False, True + 1, False * 0.1
(2, 1, 0, 2, 0.0)
>>> True / False   # python2中False为0（Python3继承该特性）
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: division by zero
```

| 数据类型           | 说明                                                         | 示例                         |
| ------------------ | ------------------------------------------------------------ | ---------------------------- |
| **整型（int）**    | 通常被称为是整型或整数，是正或负整数。Python3 整型是没有限制大小的，可以当作 Long 类型使用，所以 Python3 没有 Python2 的 Long 类型。 |                              |
| **浮点型(float)**  | 浮点型由整数部分与小数部分组成，浮点型也可以使用科学计数法表示（2.5e2 = 2.5 x 102 = 250） —— 更多内容参看[浮点数算法：争议和限制](http://www.pythondoc.com/pythontutorial3/floatingpoint.html) - [Python tutorial 3.6.3 中文手册](http://www.pythondoc.com/pythontutorial3/index.html) | 5.5                          |
| **布尔型(bool)**   | 布尔类型为真或为假，可以用 `True` 和 `False` 常量表示，也可以通过布尔运算表示。Python 2中布尔值可以当做数值对待，`True` 为 `1`；`False` 为 0 。（不要尝试True/False，会报错的~） | True<br />False<br />a and b |
| **复数( complex)** | 复数由实数部分和虚数部分构成，可以用a + bj,或者complex(a,b)表示， 复数的实部a和虚部b都是浮点型。 | 4+3j                         |

> **注意：**long 类型只存在于 Python2.X 版本中，在 2.2 以后的版本中，int 类型数据溢出后会自动转为long类型。在 Python3.X 版本中 long 类型被移除，使用 int 替代。
>
> **注意：**Python2.x 里，整数除整数，返回为整数。如果要得到小数部分，把其中一个数改成浮点数即可（自动进行了类型转换）。Python 3中则整数相除时，直接装换为浮点类型。
>
> **注意：**在 Python2 中是没有布尔型的，它用数字 0 表示 False，用 1 表示 True。到 Python3 中，把 True 和 False 定义成关键字了，但它们的值还是 1 和 0，它们可以和数字类型做算术运算。

---

####  数学函数

大部分函数在 `math` 包内，使用时，导入 `math` 包： `import math` (或 `from math imoprt xxx`) ，更多模块导入方式查看[此文](http://codingpy.com/article/python-import-101/)

| 函数                                                         | 描述说明                                                     | 示例                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ----------------------------------------------------- |
| [abs(x)](http://www.runoob.com/python3/python3-func-number-abs.html) | 返回数字的绝对值                                             | abs(-10) 返回 10                                      |
| [ceil(x)](http://www.runoob.com/python3/python3-func-number-ceil.html) | 返回数字的上入整数                                           | math.ceil(4.1) 返回 5                                 |
| cmp(x, y)                                                    | 如果 x < y 返回 -1, 如果 x == y 返回 0, 如果 x > y 返回 1。 **Python 3 已废弃** 。使用 **使用 (x>y)-(x<y)** 替换。 |                                                       |
| [exp(x)](http://www.runoob.com/python3/python3-func-number-exp.html) | 返回e的x次幂(ex)                                             | math.exp(1) 返回2.718281828459045                     |
| [fabs(x)](http://www.runoob.com/python3/python3-func-number-fabs.html) | 返回数字的绝对值                                             | math.fabs(-10) 返回10.0                               |
| [floor(x)](http://www.runoob.com/python3/python3-func-number-floor.html) | 返回数字的下舍整数                                           | math.floor(4.9)返回 4                                 |
| [log(x)](http://www.runoob.com/python3/python3-func-number-log.html) | 返回x的自然对数                                              | math.log(math.e)返回1.0,<br />math.log(100,10)返回2.0 |
| [log10(x)](http://www.runoob.com/python3/python3-func-number-log10.html) | 返回以10为基数的x的对数                                      | math.log10(100)返回 2.0                               |
| [max(x1, x2,...)](http://www.runoob.com/python3/python3-func-number-max.html) | 返回给定参数的最大值，参数可以为序列。                       | max([1,2,3,4]) 返回 4                                 |
| [min(x1, x2,...)](http://www.runoob.com/python3/python3-func-number-min.html) | 返回给定参数的最小值，参数可以为序列。                       | min([1,2,3,4]) 返回 1                                 |
| [modf(x)](http://www.runoob.com/python3/python3-func-number-modf.html) | 返回x的整数部分与小数部分，两部分的数值符号与x相同，整数部分以浮点型表示。 | math.modf(123.456) 返回(0.45600000000000307, 123.0)   |
| [pow(x, y)](http://www.runoob.com/python3/python3-func-number-pow.html) | x**y 运算后的值。                                            | pow(2,10) 返回 1024                                   |
| [round(x [,n\])](http://www.runoob.com/python3/python3-func-number-round.html) | 返回浮点数x的四舍五入值，如给出n值，则代表舍入到小数点后的位数。 | round(4.119, 2) 返回 4.12                             |
| [sqrt(x)](http://www.runoob.com/python3/python3-func-number-sqrt.html) | 返回数字x的平方根。                                          | math.sqrt(100) 返回 10.0                              |

---

#### 随机数函数

随机数可以用于数学，游戏，安全等领域中，还经常被嵌入到算法中，用以提高算法效率，并提高程序的安全性。

Python包含以下常用随机数函数：

| 函数                                                         | 描述                                                         | 示例                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [choice(seq)](http://www.runoob.com/python3/python3-func-number-choice.html) | 从序列的元素中随机挑选一个元素                               | random.choice(range(10))，从0到9中随机挑选一个整数。         |
| [randrange ([start,\] stop [,step])](http://www.runoob.com/python3/python3-func-number-randrange.html) | 从指定范围内，按指定基数递增的集合中获取一个随机数，基数缺省值为1 | random.randrange(2, 10, 2) 返回 4                            |
| [random()](http://www.runoob.com/python3/python3-func-number-random.html) | 随机生成下一个实数，它在[0,1)范围内。                        | random.random() 返回 0.3439615715984853                      |
| [seed([x])](http://www.runoob.com/python3/python3-func-number-seed.html) | 改变随机数生成器的种子seed。如果你不了解其原理，你不必特别去设定seed，Python会帮你选择seed。 |                                                              |
| [shuffle(lst)](http://www.runoob.com/python3/python3-func-number-shuffle.html) | 将序列的所有元素随机排序                                     | list = range(10)
random.shuffle(list)
print(list)

返回 [0, 8, 1, 9, 3, 6, 7, 4, 5, 2] |
| [uniform(x, y)](http://www.runoob.com/python3/python3-func-number-uniform.html) | 随机生成下一个实数，它在[x,y]范围内。                        | random.uniform(1,10)
返回 6.317586191471047                   |

---

#### 三角函数

Python包括以下三角函数：

| 函数                                                         | 描述                                              |
| ------------------------------------------------------------ | ------------------------------------------------- |
| [acos(x)](http://www.runoob.com/python3/python3-func-number-acos.html) | 返回x的反余弦弧度值。                             |
| [asin(x)](http://www.runoob.com/python3/python3-func-number-asin.html) | 返回x的反正弦弧度值。                             |
| [atan(x)](http://www.runoob.com/python3/python3-func-number-atan.html) | 返回x的反正切弧度值。                             |
| [atan2(y, x)](http://www.runoob.com/python3/python3-func-number-atan2.html) | 返回给定的 X 及 Y 坐标值的反正切值。              |
| [cos(x)](http://www.runoob.com/python3/python3-func-number-cos.html) | 返回x的弧度的余弦值。                             |
| [hypot(x, y)](http://www.runoob.com/python3/python3-func-number-hypot.html) | 返回欧几里德范数 sqrt(x*x + y*y)。                |
| [sin(x)](http://www.runoob.com/python3/python3-func-number-sin.html) | 返回的x弧度的正弦值。                             |
| [tan(x)](http://www.runoob.com/python3/python3-func-number-tan.html) | 返回x弧度的正切值。                               |
| [degrees(x)](http://www.runoob.com/python3/python3-func-number-degrees.html) | 将弧度转换为角度,如degrees(math.pi/2) ， 返回90.0 |
| [radians(x)](http://www.runoob.com/python3/python3-func-number-radians.html) | 将角度转换为弧度                                  |

---

#### 数学常量

| 常量 | 描述                                  |
| ---- | ------------------------------------- |
| pi   | 数学常量 pi（圆周率，一般以π来表示）  |
| e    | 数学常量 e，e即自然常数（自然常数）。 |

---

### String 字符串类型

字符串是 Python 中最常用的数据类型。我们可以使用引号('或")来创建字符串。Python 不支持单字符类型，单字符在 Python 中也是作为一个字符串使用。Python 访问子字符串，可以使用方括号来截取字符串。

```python
>>> var = 'Hello World!'
>>> print ("var1[0]: ", var[0])
>>> var1[0]:  H
>>> print ("var2[1:5]: ", var[1:5])
>>> var2[1:5]:  ello
```

更多内容参看

- [Python学习笔记 -- 序列（二）字符串](https://blog.csdn.net/u012995964/article/details/46440107)
- [Python学习笔记-- 字符串和数字的连接](https://blog.csdn.net/u012995964/article/details/45096547)
- [Python中单、双引号及多引号区别](https://blog.csdn.net/u012995964/article/details/45095479)

---

#### 转义字符

在需要在字符中使用特殊字符时，python用反斜杠(\)转义字符。如下表：

| 转义字符    | 描述                                         |
| ----------- | -------------------------------------------- |
| \(在行尾时) | 续行符                                       |
| \\          | 反斜杠符号                                   |
| \'          | 单引号                                       |
| \"          | 双引号                                       |
| \a          | 响铃                                         |
| \b          | 退格(Backspace)                              |
| \e          | 转义                                         |
| \000        | 空                                           |
| \n          | 换行                                         |
| \v          | 纵向制表符                                   |
| \t          | 横向制表符                                   |
| \r          | 回车                                         |
| \f          | 换页                                         |
| \oyy        | 八进制数，yy代表的字符，例如：\o12代表换行   |
| \xyy        | 十六进制数，yy代表的字符，例如：\x0a代表换行 |
| \other      | 其它的字符以普通格式输出                     |

---

#### 字符串运算符

假设变量： **a='Hello'，b='Python'**：

| 操作符 | 描述                                                         | 实例                                        |
| ------ | ------------------------------------------------------------ | ------------------------------------------- |
| +      | 字符串连接                                                   | a + b 返回： **HelloPython**                |
| *      | 重复输出字符串                                               | a*2 返回：**HelloHello**                    |
| []     | 通过索引获取字符串中字符                                     | a[1] 返回： **e**                           |
| [ : ]  | 截取字符串中的一部分，遵循**左闭右开**原则，str[0,2] 是不包含第 3 个字符的。 | a[1:4] 返回： **ell**                       |
| in     | 成员运算符 - 如果字符串中包含给定的字符返回 True             | **'H' in a** 返回 ：True                    |
| not in | 成员运算符 - 如果字符串中不包含给定的字符返回 True           | **'M' not in a** 返回： True                |
| r/R    | 原始字符串 - 原始字符串：所有的字符串都是直接按照字面的意思来使用，没有转义特殊或不能打印的字符。 原始字符串除在字符串的第一个引号前加上字母 r（可以大小写）以外，与普通字符串有着几乎完全相同的语法。 | `print(r'\n')或 print(R'\n')` 返回： **\n** |
| %      | 格式字符串                                                   | [参看此处](./format_characters.md)          |

---

#### 字符串内建函数

Python 的字符串常用内建函数如下：

| 方法                                                         | 方法描述                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [capitalize()](http://www.runoob.com/python3/python3-string-capitalize.html) | 将字符串的第一个字符转换为大写                               |
| [center(width, fillchar)](http://www.runoob.com/python3/python3-string-center.html) | 返回一个指定的宽度 width 居中的字符串，fillchar 为填充的字符，默认为空格。 |
| [count(str, beg= 0,end=len(string))](http://www.runoob.com/python3/python3-string-count.html) | 返回 str 在 string 里面出现的次数，如果 beg 或者 end 指定则返回指定范围内 str 出现的次数 |
| [bytes.decode(encoding="utf-8", errors="strict")](http://www.runoob.com/python3/python3-string-decode.html) | Python3 中没有 decode 方法，但我们可以使用 bytes 对象的 decode() 方法来解码给定的 bytes 对象，这个 bytes 对象可以由 str.encode() 来编码返回。 |
| [encode(encoding='UTF-8',errors='strict')](http://www.runoob.com/python3/python3-string-encode.html) | 以 encoding 指定的编码格式编码字符串，如果出错默认报一个ValueError 的异常，除非 errors 指定的是'ignore'或者'replace' |
| [endswith(suffix, beg=0, end=len(string))](http://www.runoob.com/python3/python3-string-endswith.html) | 检查字符串是否以 obj 结束，如果beg 或者 end 指定则检查指定的范围内是否以 obj 结束，如果是，返回 True,否则返回 False. |
| [expandtabs(tabsize=8)](http://www.runoob.com/python3/python3-string-expandtabs.html) | 把字符串 string 中的 tab 符号转为空格，tab 符号默认的空格数是 8 。 |
| [find(str, beg=0 end=len(string))](http://www.runoob.com/python3/python3-string-find.html) | 检测 str 是否包含在字符串中，如果指定范围 beg 和 end ，则检查是否包含在指定范围内，如果包含返回开始的索引值，否则返回-1 |
| [index(str, beg=0, end=len(string))](http://www.runoob.com/python3/python3-string-index.html) | 跟find()方法一样，只不过如果str不在字符串中会报一个异常.     |
| [isalnum()](http://www.runoob.com/python3/python3-string-isalnum.html) | 如果字符串至少有一个字符并且所有字符都是字母或数字则返 回 True,否则返回 False |
| [isalpha()](http://www.runoob.com/python3/python3-string-isalpha.html) | 如果字符串至少有一个字符并且所有字符都是字母则返回 True, 否则返回 False |
| [isdigit()](http://www.runoob.com/python3/python3-string-isdigit.html) | 如果字符串只包含数字则返回 True 否则返回 False..             |
| [islower()](http://www.runoob.com/python3/python3-string-islower.html) | 如果字符串中包含至少一个区分大小写的字符，并且所有这些(区分大小写的)字符都是小写，则返回 True，否则返回 False |
| [isnumeric()](http://www.runoob.com/python3/python3-string-isnumeric.html) | 如果字符串中只包含数字字符，则返回 True，否则返回 False      |
| [isspace()](http://www.runoob.com/python3/python3-string-isspace.html) | 如果字符串中只包含空白，则返回 True，否则返回 False.         |
| [istitle()](http://www.runoob.com/python3/python3-string-istitle.html) | 如果字符串是标题化的(见 title())则返回 True，否则返回 False  |
| [isupper()](http://www.runoob.com/python3/python3-string-isupper.html) | 如果字符串中包含至少一个区分大小写的字符，并且所有这些(区分大小写的)字符都是大写，则返回 True，否则返回 False |
| [join(seq)](http://www.runoob.com/python3/python3-string-join.html) | 以指定字符串作为分隔符，将 seq 中所有的元素(的字符串表示)合并为一个新的字符串 |
| [len(string)](http://www.runoob.com/python3/python3-string-len.html) | 返回字符串长度                                               |
| [ljust(width[, fillchar])](http://www.runoob.com/python3/python3-string-ljust.html) | 返回一个原字符串左对齐,并使用 fillchar 填充至长度 width 的新字符串，fillchar 默认为空格。 |
| [lower()](http://www.runoob.com/python3/python3-string-lower.html) | 转换字符串中所有大写字符为小写.                              |
| [lstrip()](http://www.runoob.com/python3/python3-string-lstrip.html) | 截掉字符串左边的空格或指定字符。                             |
| [maketrans()](http://www.runoob.com/python3/python3-string-maketrans.html) | 创建字符映射的转换表，对于接受两个参数的最简单的调用方式，第一个参数是字符串，表示需要转换的字符，第二个参数也是字符串表示转换的目标。 |
| [max(str)](http://www.runoob.com/python3/python3-string-max.html) | 返回字符串 str 中最大的字母。                                |
| [min(str)](http://www.runoob.com/python3/python3-string-min.html) | 返回字符串 str 中最小的字母。                                |
| [replace(old, new [, max])](http://www.runoob.com/python3/python3-string-replace.html) | 把 将字符串中的 str1 替换成 str2,如果 max 指定，则替换不超过 max 次。 |
| [rfind(str, beg=0,end=len(string))](http://www.runoob.com/python3/python3-string-rfind.html) | 类似于 find()函数，不过是从右边开始查找.                     |
| [rindex( str, beg=0, end=len(string))](http://www.runoob.com/python3/python3-string-rindex.html) | 类似于 index()，不过是从右边开始.                            |
| [rjust(width,[, fillchar])](http://www.runoob.com/python3/python3-string-rjust.html) | 返回一个原字符串右对齐,并使用fillchar(默认空格）填充至长度 width 的新字符串 |
| [rstrip()](http://www.runoob.com/python3/python3-string-rstrip.html) | 删除字符串字符串末尾的空格.                                  |
| [split(str="", num=string.count(str))](http://www.runoob.com/python3/python3-string-split.html) | num=string.count(str)) 以 str 为分隔符截取字符串，如果 num 有指定值，则仅截取 num 个子字符串 |
| [splitlines([keepends])](http://www.runoob.com/python3/python3-string-splitlines.html) | 按照行('\r', '\r\n', \n')分隔，返回一个包含各行作为元素的列表，如果参数 keepends 为 False，不包含换行符，如果为 True，则保留换行符。 |
| [startswith(str, beg=0,end=len(string))](http://www.runoob.com/python3/python3-string-startswith.html) | 检查字符串是否是以 obj 开头，是则返回 True，否则返回 False。如果beg 和 end 指定值，则在指定范围内检查。 |
| [strip([chars])](http://www.runoob.com/python3/python3-string-strip.html) | 在字符串上执行 lstrip()和 rstrip()                           |
| [swapcase()](http://www.runoob.com/python3/python3-string-swapcase.html) | 将字符串中大写转换为小写，小写转换为大写                     |
| [title()](http://www.runoob.com/python3/python3-string-title.html) | 返回"标题化"的字符串,就是说所有单词都是以大写开始，其余字母均为小写(见 istitle()) |
| [translate(table, deletechars="")](http://www.runoob.com/python3/python3-string-translate.html) | 根据 str 给出的表(包含 256 个字符)转换 string 的字符, 要过滤掉的字符放到 deletechars 参数中 |
| [upper()](http://www.runoob.com/python3/python3-string-upper.html) | 转换字符串中的小写字母为大写                                 |
| [zfill (width)](http://www.runoob.com/python3/python3-string-zfill.html) | 返回长度为 width 的字符串，原字符串右对齐，前面填充0         |
| [isdecimal()](http://www.runoob.com/python3/python3-string-isdecimal.html) | 检查字符串是否只包含十进制字符，如果是返回 true，否则返回 false。 |

---

### Tuple 元组类型

元组与列表类似，不同之处在于元组的元素不能修改。元组使用小括号，列表使用方括号。

```python
# 元组 及 元组操作
>>> new_tuple = (0, True, -0.001, 1, 9, 2, 'hello', [1, 2, 3], ('a', 'b'))
>>> new_tuple
(0, True, -0.001, 1, 9, 2, 'hello', [1, 2, 3], ('a', 'b'))
>>> new_tuple[2:6]
(-0.001, 1, 9, 2)
>>> new_tuple[:-1]
(0, True, -0.001, 1, 9, 2, 'hello', [1, 2, 3])
>>> new_tuple[7][1]
2

# 字面量创建只含有一个数值类型元素的元组时，元素后跟一个逗号，用于区分运算符和元组标识符
>>> (1); type(1)	# 不加逗号，则创建的是数值类型
1
<class 'int'>
>>> (1,); type((1,))  #加逗号，创建的是元组
(1,)
<class 'tuple'>
>>> empty_tuple = (); empty_tuple; type(empty_tuple)	# 创建空序列
()
<class 'tuple'>

# tuple 函数
>>> tuple(range(10))
(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
>>> tuple([1,2,3,4])
(1, 2, 3, 4)
>>> tuple({'a':1,'b':2}) 	# 字典key转tuple
('a', 'b')
>>> tuple({'a':1,'b':2}.keys())
('a', 'b')
>>> tuple({'a':1,'b':2}.values()) #字典value转tuple
(1, 2)

# tuple 值改变
>>> new_tuple[0] = 100		# tuple类型是不可变的，不支持元素的修改
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
    
>>> new_tuple.append({'a':1})	# tuple没有append类型
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'tuple' object has no attribute 'append'

>>> tmp_tuple = new_tuple
>>> id(tmp_tuple); id(new_tuple)
1935881543992
1935881543992
>>> id(tmp_tuple); tmp_tuple = tmp_tuple[:5]; id(a)  # 元组的更改，会创建新的元组（新的引用）
1935881543992
1935881230032

# list与tuple共有的属性方法（交集）
>>> list(set(dir(tuple)).intersection(set(dir(list))))
['index', '__getattribute__', '__init_subclass__', '__hash__', 'count', '__sizeof__', '__eq__', '__subclasshook__', '__le__', '__mul__', '__ge__', '__getitem__', '__len__', '__format__', '__contains__', '__init__', '__reduce__', '__doc__', '__delattr__', '__reduce_ex__', '__rmul__', '__iter__', '__setattr__', '__repr__', '__new__', '__add__', '__ne__', '__dir__', '__str__', '__lt__', '__class__', '__gt__']
# list中有，tuple中没有的属性方法（差集）
>>> list(set(dir(list)).difference(set(dir(tuple))))
['extend', '__reversed__', 'remove', '__imul__', 'append', '__iadd__', 'reverse', 'clear', 'pop', 'copy', 'insert', 'sort', '__setitem__', '__delitem__']

```

#### 元组内置函数

Python元组包含了以下内置函数

| 方法       | 描述说明               | 示例                                                         |
| --- | --- | --- |
| len(tuple) | 计算元组元素个数。     | len((1,10,5,2,-1,0)) 返回 6 |
| max(tuple) | 返回元组中元素最大值。 | max((1,10,5,2,-1,0)) 返回 10 |
| min(tuple) | 返回元组中元素最小值。 | min((1,10,5,2,-1,0)) 返回 -1    |
| tuple(seq) | 将列表转换为元组。     | tuple([1,2,3,4]) 返回 (1,2,3,4) |

---

### List 列表类型

序列是Python中最基本的数据结构。序列中的每个元素都分配一个数字 - 它的位置，或索引，第一个索引是0，第二个索引是1，依此类推。列表的数据项不需要具有相同的类型。

```python
# 列表 及 切片操作
>>> new_list = [0, True, -0.001, 0x01, 0o11, 0b010, 'hello', [1,2,3], ('a','b')]
>>> new_list
[0, True, -0.001, 1, 9, 2, 'hello', [1, 2, 3], ('a', 'b')]
>>> new_list[2:6]
[-0.001, 1, 9, 2]
>>> new_list[:-1]
[0, True, -0.001, 1, 9, 2, 'hello', [1, 2, 3]]
>>> new_list[7][1]
2

# list函数
>>> list(range(10))
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> list((1,2,3,4))	#tuple转list
[1, 2, 3, 4]
>>> list({1:1,2:2})	
[1, 2]
>>> list({'a':1,'b':2})	# 或 list({'a':1,'b':2}.keys()) 字典key转list 
['a', 'b']
>>> list({'a':1,'b':2}.values())	#字典value转list
[1, 2]

# list 值改变
>>> print('new_list={}, id={}'.format(new_list, id(new_list))); new_list[7] = [3,4,5]; print('new_list={}, id={}'.format(new_list, id(new_list)));
new_list=[0, True, -0.001, 1, 9, 2, 'hello', [1, 2, 3], ('a', 'b')], id=1898243111112
new_list=[0, True, -0.001, 1, 9, 2, 'hello', [3, 4, 5], ('a', 'b')], id=1898243111112

# list 值删除(del语句)
>>> print('num_list={}, id={}'.format(num_list, id(num_list))); del num_list[7]; print('num_list={}, id={}'.format(num_list, id(num_list)));	## 此处使用del 完成删除
num_list=[0, True, -0.001, 1, 9, 2, 'hello', [3, 4, 5], ('a', 'b')], id=1898243111112
num_list=[0, True, -0.001, 1, 9, 2, 'hello', ('a', 'b')], id=1898243111112
```

---

#### 列表操作符

列表对 + 和 * 的操作符与字符串相似。+ 号用于组合列表，* 号用于重复列表。如下所示：

| 表达式                | 说明                     | 结果                         |
| --------------------- | ------------------------ | ---------------------------- |
| len([1, 2, 3])        | 返回列表长度             | 3                            |
| [1, 2, 3] + [4, 5, 6] | 合并两个列表             | [1, 2, 3, 4, 5, 6]           |
| ['Hi!'] * 4           | 列表值循环               | ['Hi!', 'Hi!', 'Hi!', 'Hi!'] |
| 3 in [1, 2, 3]        | 判断元素是否存在于列表中 | True                         |

---

#### 列表函数&方法

Python包含以下可以操作列表的函数，以变量 **list = [2, 4, 1, 3]**:

| 内置函数                                                     | 说明               | 示例              |
| ------------------------------------------------------------ | ------------------ | ----------------- |
| [len(list)](http://www.runoob.com/python3/python3-att-list-len.html) | 列表元素个数       | len(list) 返回：4 |
| [max(list)](http://www.runoob.com/python3/python3-att-list-max.html) | 返回列表元素最大值 | max(list) 返回：4 |
| [min(list)](http://www.runoob.com/python3/python3-att-list-min.html) | 返回列表元素最小值 |                   |
| [list(seq)](http://www.runoob.com/python3/python3-att-list-list.html) | 将元组转换为列表   |                   |

Python包含以下方法:

| 序号                                                         | 方法                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [list.append(obj)](http://www.runoob.com/python3/python3-att-list-append.html) | 在列表末尾添加新的对象                                       |
| [list.count(obj)](http://www.runoob.com/python3/python3-att-list-count.html) | 统计某个元素在列表中出现的次数                               |
| [list.extend(seq)](http://www.runoob.com/python3/python3-att-list-extend.html) | 在列表末尾一次性追加另一个序列中的多个值（用新列表扩展原来的列表） |
| [list.index(obj)](http://www.runoob.com/python3/python3-att-list-index.html) | 从列表中找出某个值第一个匹配项的索引位置                     |
| [list.insert(index, obj)](http://www.runoob.com/python3/python3-att-list-insert.html) | 将对象插入列表                                               |
| [list.pop([index=-1])](http://www.runoob.com/python3/python3-att-list-pop.html) | 移除列表中的一个元素（默认最后一个元素），并且返回该元素的值 |
| [list.remove(obj)](http://www.runoob.com/python3/python3-att-list-remove.html) | 移除列表中某个值的第一个匹配项                               |
| [list.reverse()](http://www.runoob.com/python3/python3-att-list-reverse.html) | 反向列表中元素                                               |
| [list.sort(cmp=None, key=None, reverse=False)](http://www.runoob.com/python3/python3-att-list-sort.html) | 对原列表进行排序                                             |
| [list.clear()](http://www.runoob.com/python3/python3-att-list-clear.html) | 清空列表                                                     |
| [list.copy()](http://www.runoob.com/python3/python3-att-list-copy.html) | 复制列表                                                     |

---

### Dictionary字典类型

字典是另一种可变容器模型，且可存储任意类型对象。字典的元素用键值对（key:value）的形式存储。

---

### Set 集合类型