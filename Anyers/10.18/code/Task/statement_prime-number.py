# -- coding: utf-8 --

'''
	1、掌握 Python 的 if-else 分支结构以及 while for 循环（包括 continue 和 break)
​	2、编写一个程序，该程序可以从命令行接收一个数字输入并判断该数字是否为素数。

'''

number = 7

def digital_guessing_if_real():
	try:
		guess = int(input("请输入你猜的数字(整数)："))
		if guess == number:
			print("恭喜你，猜对了！！！")
		elif guess < number:
			print("猜的数字小了...")
			digital_guessing_if_real()
		elif guess > number:
			print("猜的数字大了...")
			digital_guessing_if_real()
	except Exception as e:
		print("程序执行异常：{}".format(e))

def digital_guessing_while_real():
	try:
		while True:
			guess = int(input("请输入你猜的数字(整数)："))
			if guess == number:
				print("恭喜你，猜对了！！！")
				break;
			elif guess < number:
				print("猜的数字小了...")
			elif guess > number:
				print("猜的数字大了...")
	except Exception as e:
		print("程序执行异常：{}".format(e))

def digital_guessing_for_real():
	try:
		for i in [1]:
			guess = int(input("请输入你猜的数字(整数)："))
			if guess == number:
				print("恭喜你，猜对了！！！")
				break;
			elif guess < number:
				print("猜的数字小了...")
				continue;
			elif guess > number:
				print("猜的数字大了...")
				continue;
		else:
			digital_guessing_for_real()
	except Exception as e:
		print("程序执行异常：{}".format(e))

#	判断素数概念 质数又称素数。 指在一个大于1的自然数中，除了1和此整数自身外，不能被其他自然数整除的数。
def prime_number():
	try:
		num = int(input("请输入一个数字(整数)："))
		if num > 1:
			for i in range(2, num):
				if num % i == 0:
					print(f"您输入的 : {num} ，不是素（质）数")
					break
			else:
				print(f"您输入的 : {num} ，是素（质）数")
		else:
			print(f"您输入的 : {num} ，不是素（质）数")
	except Exception as e:
		print("程序执行异常：{}".format(e))

# if_digital_guessing()
# digital_guessing_while_real()
# digital_guessing_for_real()
prime_number()

