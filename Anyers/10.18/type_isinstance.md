## Python中type与isinstance

### type()

 `class type(object)` ``class type(name, bases, dict)` 

使用一个参数，返回*对象的类型*。返回值是一个类型对象，并且通常与[`object.__class__`](https://yiyibooks.cn/__trs__/xx/python_352/library/stdtypes.html#instance.__class__)返回的对象相同。建议使用[`isinstance()`](https://yiyibooks.cn/__trs__/xx/python_352/library/functions.html#isinstance)内置函数来测试对象的类型，因为它考虑了子类。有三个参数，返回一个新的类型对象。这本质上是[`class`](https://yiyibooks.cn/__trs__/xx/python_352/reference/compound_stmts.html#class)语句的动态形式。

```python
>>> a, b, c, d = 20, 5.5, True, 4+3j
>>> print(type(a), type(b), type(c), type(d))
<class 'int'> <class 'float'> <class 'bool'> <class 'complex'>
>>> a = 123; 
>>> type(a); a.__class__
<class 'int'>
<class 'int'>
```

---

### isinstance()

`isinstance(object, classinfo)`

如果*object*是*clsaainfo*的一个实例（或者是classinfo的直接、间接或[虚拟](https://yiyibooks.cn/__trs__/xx/python_352/glossary.html#term-abstract-base-class)子类的实例），那么则返回true。如果*object*不是给定类型的对象，则该函数始终返回false。如果*classinfo*是类型对象的元组（或者其他这样的元组），如果*object*是任何类型的实例，则返回true。如果*classinfo*不是类型或类型组成的元祖和此类元组，则会引发[`TypeError`](https://yiyibooks.cn/__trs__/xx/python_352/library/exceptions.html#TypeError)异常。



---

**type()** 与  **isinstance()** 区别：

- type() 不会认为子类是一种父类类型，不考虑继承关系。
- isinstance() 会认为子类是一种父类类型，考虑继承关系。

如果要判断两个类型是否相同推荐使用 **isinstance()**。

```python
class Person(object):   # 定义一个父类

    def talk(self):    # 父类中的方法
        print("person is talking....")  
 
class Chinese(Person):    # 定义一个子类， 继承Person类
 
    def walk(self):      # 在子类中定义其自身的方法
        print('is walking...')


```



