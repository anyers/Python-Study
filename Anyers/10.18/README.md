# 【战胜拖延症，组团学 Python】- 第四天

## [今日打卡](https://juejin.im/pin/5bc7e7bbf265da6aedfc8759)

>【战胜拖延症，组团学 Python】第二课
>
>​	1、掌握 Python 的 if-else 分支结构以及 while for 循环（包括 continue 和 break)
>
>​	2、编写一个程序，该程序可以从命令行接收一个数字输入并判断该数字是否为素数。
>
> 阅读链接中的 Python 基础部分，配合搜索可以完成此任务。[Python3教程 - 廖雪峰](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000)

### 任务1

```python
# -- coding: utf-8 --
number = 7

def digital_guessing_if_real():
	try:
		guess = int(input("请输入你猜的数字(整数)："))
		if guess == number:
			print("恭喜你，猜对了！！！")
		elif guess < number:
			print("猜的数字小了...")
			digital_guessing_if_real()
		elif guess > number:
			print("猜的数字大了...")
			digital_guessing_if_real()
	except Exception as e:
		print("程序执行异常：{}".format(e))

def digital_guessing_while_real():
	try:
		while True:
			guess = int(input("请输入你猜的数字(整数)："))
			if guess == number:
				print("恭喜你，猜对了！！！")
				break;
			elif guess < number:
				print("猜的数字小了...")
			elif guess > number:
				print("猜的数字大了...")
	except Exception as e:
		print("程序执行异常：{}".format(e))

def digital_guessing_for_real():
	try:
		for i in [1]:
			guess = int(input("请输入你猜的数字(整数)："))
			if guess == number:
				print("恭喜你，猜对了！！！")
				break;
			elif guess < number:
				print("猜的数字小了...")
				continue;
			elif guess > number:
				print("猜的数字大了...")
				continue;
		else:
			digital_guessing_for_real()
	except Exception as e:
		print("程序执行异常：{}".format(e))

# if_digital_guessing()
# digital_guessing_while_real()
# digital_guessing_for_real()
```



---

### 任务2

```python
# -- coding: utf-8 --
#	判断素数概念 质数又称素数。 指在一个大于1的自然数中，除了1和此整数自身外，不能被其他自然数整除的数。
def prime_number():
	try:
		num = int(input("请输入一个数字(整数)："))
		if num > 1:
			for i in range(2, num):
				if num % i == 0:
					print(f"您输入的 : {num} ，不是质数")
					break
			else:
				print(f"您输入的 : {num} ，是质数")
		else:
			print(f"您输入的 : {num} ，不是质数")

	except Exception as e:
		print("程序执行异常：{}".format(e))

prime_number()
```



---

### 阅读参考

- [Python3教程 - 廖雪峰](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000)
- [浮点数算法：争议和限制](http://www.pythondoc.com/pythontutorial3/floatingpoint.html) - [Python tutorial 3.6.3 中文手册](http://www.pythondoc.com/pythontutorial3/index.html)