# Python-Study

#### 项目介绍

该项目为掘金[「战胜拖延症，组团学 Python」](https://juejin.im/post/5bbdd6896fb9a05ce02a9902)活动，瑟瑟发抖战队，用于Python学习打卡、问题及代码记录使用。希望可以一起坚持，共同进步。Come on, fight together~~

#### 战队情况

- [Anyers](./Anyers)
- [soufal](./soufal)

#### 扩展学习

- [Learn Git Branching]( https://learngitbranching.js.org/) - 在沙盒里你能执行相应的命令，还能看到每个命令的执行情况； 通过一系列刺激的关卡挑战，逐步深入的学习 Git 的强大功能，在这个过程中你可能还会发现一些有意思的事情。

